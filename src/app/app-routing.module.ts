import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DivisionesComponent } from './components/routes/divisiones/divisiones.component';
import { EquiposComponent } from './components/routes/equipos/equipos.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { JugadoresComponent } from './components/routes/jugadores/jugadores.component';
import { PlantelesComponent } from './components/routes/planteles/planteles.component';


const routes: Routes = [
  {
    path:'Inicio',
    component: InicioComponent
  },
  {
    path:'Equipos',
    component: EquiposComponent
  },
  {
    path:'Mjugadores',
    component: JugadoresComponent
  },
  {
    path:'Divisiones',
    component: DivisionesComponent
  },
  {
    path:'planteles',
    component: PlantelesComponent
  },
  {
    path:'**',
    redirectTo: 'Inicio'
  },
  { 
    path: '',   
    redirectTo: '/Inicio', 
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
