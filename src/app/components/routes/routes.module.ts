import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { EquiposComponent } from './equipos/equipos.component';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { DivisionesComponent } from './divisiones/divisiones.component';
import { PlantelesComponent } from './planteles/planteles.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    InicioComponent,
    EquiposComponent,
    JugadoresComponent,
    DivisionesComponent,
    PlantelesComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class RoutesModule { }
